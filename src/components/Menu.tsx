import { MenuProps } from "antd";
import * as antd from "antd";
import { usePathname } from "next/navigation";
import { RouteProps } from "react-router-dom";

type Elem<A> = A extends readonly (infer T)[] ? T : never;

export default function Menu({ routes }: { routes: (Elem<MenuProps['items']> & RouteProps)[] }) {
    const path = '/' + usePathname().split('/')[1] || ''
    const route = routes.find(r => r.path === path)

    return (
        <antd.Menu
            theme='dark'
            mode='inline'
            selectable={false}
            style={{ display: 'flex', flexFlow: 'column', height: '100%' }}
            items={routes}
            selectedKeys={route ? [`${route.key}`] : []}
            />
    )
}