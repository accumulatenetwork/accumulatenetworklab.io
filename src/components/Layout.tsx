'use client'
import Menu from "./Menu";
import React from "react";
import styles from './Layout.module.css';
import { ConfigProvider, Layout } from "antd";

interface Props {
    routes: Parameters<typeof Menu>[0]['routes']
    children: React.ReactNode
}

export default function MyLayout({ routes, children }: Props) {
    return (
        <ConfigProvider
            theme={{
                token: {
                    fontFamily: `-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif`
                }
            }}>
            <Layout className={styles.layout}>
                <Layout.Sider>
                    <Menu routes={routes} />
                </Layout.Sider>
                <Layout.Content className={styles.main}>
                    {children}
                </Layout.Content>
            </Layout>
        </ConfigProvider>
    )
}