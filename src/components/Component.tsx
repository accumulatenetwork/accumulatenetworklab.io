import React from "react";

export default class Component<P, S> extends React.Component<P, S> {
    protected updateState(s: Partial<S>) {
        this.setState(Object.assign(this.state || {}, s));
    }
}