import Component from '@/components/Component';
import { Input, Table, TableProps, Typography } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

const { Link } = Typography

const glQuery = `
    query {
        project(fullPath: "accumulatenetwork/governance/aip") {
            issues(state: opened) {
                nodes {
                    iid
                    title
                    description
                    descriptionHtml
                    upvotes
                    downvotes
                    webUrl
                    notes {
                        nodes {
                            id
                        }
                    }
                }
            }
        }
    }
`

type GlIssue = {
    iid: string;
    title: string;
    description: string;
    descriptionHtml: string;
    upvotes: number;
    downvotes: number;
    webUrl: string;
    notes: {
        nodes: {}[];
    }
};

type GlResponse = {
    data: {
        project: {
            issues: {
                nodes: GlIssue[]
            }
        }
    }
};

interface State {
    issues: GlIssue[]
}

export class Features extends Component<{}, State> {
    async componentDidMount() {
        const resp = await (await fetch('https://gitlab.com/api/graphql', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ query: glQuery }),
        })).json() as GlResponse

        const { data: { project: { issues: { nodes: issues } } } } = resp
        this.updateState({ issues })
    }

    render() {
        function hasVotes(issue: GlIssue) {
            return issue.upvotes > 0 || issue.downvotes > 0
        }

        const columns: TableProps<GlIssue>['columns'] = [
            {
                title: 'Votes',
                key: 'votes',
                defaultSortOrder: 'descend',
                width: '4em',
                align: 'center',
                sorter(a: GlIssue, b: GlIssue) {
                    const aHas = hasVotes(a)
                    const bHas = hasVotes(b)
                    if (!aHas && !bHas) return 0
                    if (!aHas)          return -1
                    if (!bHas)          return +1
                    return (a.upvotes - a.downvotes) - (b.upvotes - b.downvotes)
                },
                render(_, record) {
                    if (!hasVotes(record)) {
                        return;
                    }
                    return <span>{record.upvotes - record.downvotes}</span>
                }
            },
            Table.EXPAND_COLUMN,
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'title',
                render(value: string, record: GlIssue) {
                    return <Link href={record.webUrl} target='_blank'>{value}</Link>
                },
                filterIcon(filtered) {
                    return <SearchOutlined style={{ color: filtered ? '#1677ff' : undefined }} />
                },
                filters: [{ text: 'Foo', value: 'foo' }],
                onFilter(value: string | number | boolean, record) {
                    const s = `${value}`.toLocaleLowerCase();
                    return record.title.toLocaleLowerCase().includes(s)
                        || record.description.toLocaleLowerCase().includes(s)
                },
                filterDropdown({ selectedKeys, setSelectedKeys, confirm }) {
                    let ref: NodeJS.Timeout;
                    return (
                        <Input
                            placeholder="Search title and description"
                            value={selectedKeys[0]}
                            onChange={e => {
                                setSelectedKeys(e.target.value ? [e.target.value] : [])

                                if (ref) clearTimeout(ref);
                                ref = setTimeout(() => confirm({ closeDropdown: false }), 100);
                            }}
                        />
                    )
                }
            },
        ]
        return (
            <div>
                <Table
                    columns={columns}
                    dataSource={this.state?.issues}
                    loading={!this.state?.issues}
                    rowKey="iid"
                    pagination={{
                        position: ['bottomLeft'],
                        showSizeChanger: true,
                    }}
                    expandable={{
                        rowExpandable(record) {
                            return !!record.descriptionHtml.length
                        },
                        expandedRowRender(record) {
                            return <div dangerouslySetInnerHTML={{ __html: record.descriptionHtml }} />
                        }
                    }}
                    />
            </div>
        )
    }
}