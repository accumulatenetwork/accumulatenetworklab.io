import { Input } from "antd";
import React from "react";
import Component from "../components/Component";
import { Alert } from "antd";
import { Envelope } from "accumulate.js/lib/messaging";
import { FieldNumber, consume, Field, Length } from "accumulate.js/lib/encoding";
import { Account, Signature, Transaction } from "accumulate.js/lib/core";
import styles from './Encoding.module.css';
import { TxID, URL } from "accumulate.js";


interface ObjectProps {
    object: any;
    kind: string;
}

class Object extends Component<ObjectProps, {}> {
    render() {
        const parts: React.ReactNode[] = [];

        function prefix(depth: number) {
            return '.. '.repeat(depth)
        }

        function fmt(bytes: Uint8Array) {
            return Buffer.from(bytes).toString('hex').replace(/[0-9a-f]{2}/g, s => ' ' + s).substring(1)
        }

        function render(field: Field, value: any, depth: number) {
            parts.push(
                <span className={styles.prefix}>{prefix(depth)}</span>,
                <span className={styles.bytes}>{fmt(FieldNumber.encode(field.number))}</span>,
            )
            if (field.type.embedding)
                parts.push(<span className={styles.embedding}>{' (embedding)\n'}</span>)
            else
                parts.push(<span className={styles.name}>{` ${field.name.toString()}\n`}</span>)

            if (field.type.raw) {
                const { length, value: v } = field.type.raw(value);
                parts.push(
                    <span className={styles.prefix}>{prefix(depth+1)}</span>,
                    <span className={styles.bytes}>{fmt(length)}</span>,
                    <span className={styles.length}>{` (length = ${v.length})\n`}</span>,
                    <span className={styles.prefix}>{prefix(depth+1)}</span>,
                    <span className={styles.bytes}>{fmt(v)}</span>,
                )
                if (typeof value === 'string' ||
                    value instanceof URL ||
                    value instanceof TxID)
                    parts.push(<span className={styles.value}>{` = ${value}\n`}</span>)
                else
                    parts.push(<span>{'\n'}</span>)
                return;
            }

            if (field.type.consume) {
                const { length } = field.type.encode(value);
                parts.push(
                    <span className={styles.prefix}>{prefix(depth+1)}</span>,
                    <span className={styles.bytes}>{fmt(Length.encode(length-1))}</span>,
                    <span className={styles.length}>{` (length = ${length-1})\n`}</span>,
                )
                field.type.consume(value, (field, value) => render(field, value, depth+2));
                return;
            }

            parts.push(
                <span className={styles.prefix}>{prefix(depth+1)}</span>,
                <span className={styles.bytes}>{fmt(field.type.encode(value))}</span>,
            );

            if (typeof value !== 'object')
                parts.push(<span className={styles.value}>{` = ${value}\n`}</span>)
            else
                parts.push(<span>{'\n'}</span>)
        }

        parts.push(
            <span className={styles.name}>{`${this.props.kind}\n`}</span>,
        )
        consume(this.props.object, (field, value) => render(field, value, 0));
        return <div className={styles.object}>{parts}</div>
    }
}

const example = `{ "header": { "principal": "acc://adi.acme", "initiator": "ec9f9880f7f4b0ebcebc9917644a1533af9d894e13070187720e1820d46f3cd1" }, "body": { "type": "createTokenAccount", "url": "acc://adi.acme/ACME", "tokenUrl": "acc://ACME.acme", "authorities": [ "acc://adi.acme/book" ] } }`

interface EncodingState {
    error: string | null;
    object: object | null;
    kind: string | null;
}
export class Encoding extends Component<{}, EncodingState> {
    parse(input: string) {
        try {
            const obj = JSON.parse(input);
            if (obj.signatures || obj.transaction || obj.messages)
                this.setState({ kind: 'envelope', object: new Envelope(obj), error: null });
            else if (obj.header && obj.body)
                this.setState({ kind: 'transaction', object: new Transaction(obj), error: null });
            else if (obj.type && obj.publicKey)
                this.setState({ kind: 'signature', object: Signature.fromObject(obj), error: null });
            else if (obj.type && obj.url)
                this.setState({ kind: 'account', object: Account.fromObject(obj), error: null });
            else {
                this.setState({ error: `Cannot determine the type of the object`, object: null });
            }
        } catch (error) {
            this.setState({ error: `${error}`, object: null });
        }
    }

    componentDidMount(): void {
        this.parse(example);
    }

    render() {
        let parseTimeout: NodeJS.Timer;
        const parse = (input: string) => {
            if (parseTimeout) clearTimeout(parseTimeout);
            parseTimeout = setTimeout(() => this.parse(input), 300);
        }

        return (
            <section className={styles.main}>
                <Input.TextArea
                    style={{ fontFamily: 'monospace', marginBottom: '1em' }}
                    placeholder="Input JSON"
                    autoSize
                    defaultValue={example}
                    onChange={({ target }) => parse(target.value)}
                    />
                {this.state?.error && <Alert message={this.state.error} type='error' showIcon />}
                {this.state?.object && <Object object={this.state.object} kind={this.state.kind!} />}
            </section>
        )
    }
}