'use client'
import { Encoding } from "@/views/Encoding"
import { Typography } from "antd"

const { Title } = Typography

export default function Home() {
    return <div>
        <Title level={2}>Encoding</Title>
        <Encoding />
    </div>
}
