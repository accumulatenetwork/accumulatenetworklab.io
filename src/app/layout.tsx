import { Metadata } from 'next'
import './globals.css'
import Menu from '@/components/Menu'
import Link from 'next/link'
import Layout from '@/components/Layout'

export const metadata: Metadata = {
  title: 'Accumulate Network',
  icons: {
    icon: { url: '/icon.svg', type: 'image/svg' },
    shortcut: { url: '/icon.svg', type: 'image/svg' },
  },
}

export default function RootLayout({
    children,
}: {
    children: React.ReactNode
}) {
    const routes: Parameters<typeof Menu>[0]['routes'] = [
        {
            label: <Link href="/">Feature Suggestions</Link>,
            key: 'features',
            path: '/',
        },
        {
            label: <Link href="/tools">Developer Tools</Link>,
            key: 'tools',
            path: '/tools',
        }
    ]

    return (
        <html lang="en">
            <head>
                <link rel="icon" href="/icon.svg" />
            </head>
            <body>
                <Layout routes={routes} children={children} />
            </body>
        </html>
    )
}
